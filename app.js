const createError = require('http-errors')
const express = require('express')
const path = require('path')
const cookieParser = require('cookie-parser')
const swaggerJSDoc = require('swagger-jsdoc')
const swaggerUi = require('swagger-ui-express')
serverConfig = require('./lib/initialize')
const authentication = require('./lib/auth')

const swaggerDefinition = {
  openapi: '3.0.0',
  info: {
    title: 'WD-API FOR LEARNING SPARTA FE',
    version: '1.0.0',
    description:
      'This is a REST API application made with Love.'
  },
  components: {
    securitySchemes: {
      auth: {
        type: 'http',
        scheme: 'basic',
        in: 'header'
      }
    }
  },
  security: [{
    auth: []
  }],
  servers: [
    {
      url: `http://localhost:${serverConfig.server.port}`,
      description: 'Development server'
    }
  ]
}

const cssOptions = {
  customCss: `
  .swagger-ui .topbar { display: none; }
  `
}

const options = {
  swaggerDefinition,
  // Path to the API docs
  apis: ['./routes/*.js']
}

const swaggerSpec = swaggerJSDoc(options)
const app = express()
// view engine setup
app.set('views', path.join(__dirname, 'views'))
app.set('view engine', 'pug')

app.use(express.json())
app.use(express.urlencoded({ extended: false }))
app.use(cookieParser())
app.use(express.static(path.join(__dirname, 'public')))

// app.use('/', require('./routes/index'))
// app.use(authentication)
app.use('/users', authentication, require('./routes/users'))
app.use('/login', authentication, require('./routes/login'))
app.use('/', swaggerUi.serve, swaggerUi.setup(swaggerSpec, cssOptions))

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404))
})

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message
  res.locals.error = req.app.get('env') === 'development' ? err : {}

  // render the error page
  res.status(err.status || 500)
  res.render('error')
})

module.exports = app
