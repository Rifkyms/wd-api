const express = require('express')
const router = express.Router()
serverUtils = require('../lib/Utils')
const log = serverUtils.getLogger('PostgreSQL')
const pool = require('../lib/config').pool
const { int } = require('neo4j-driver')
const neo4j = require('../lib/neo4j')

/**
 * @swagger
 * /users/all:
 *   get:
 *     tags:
 *      - Users
 *     summary: Retrieve a list of JSONPlaceholder users.
 *     description: Retrieve a list of users from JSONPlaceholder. Can be used to populate a list of fake users when prototyping or testing an API.
 *     responses:
 *       200:
 *         description: Success
 *       202:
 *         description: No Action Performed
 *       400:
 *         description: Error
 *       500:
 *         description: Internal Application Domain Error
 *       404:
 *         description: Data Not Found
 */

router.get('/all', (req, res, next) => {
  log.info('[GET] /users/all data: {}')
  pool.query(
    'SELECT * FROM users',
    (error, results) => {
      if (results.rows.length === 0) {
        log.error(error)
        res.json({
          status: 404,
          message: 'not found',
          data: results.rows
        })
      }
      res.json({
        status: 200,
        message: 'Success',
        data: results.rows
      })
    }
  )
})

/**
 * @swagger
 * /users/getUser/{id}:
 *   get:
 *     tags:
 *      - Users
 *     summary: Retrieve a single JSONPlaceholder user.
 *     description: Retrieve a single JSONPlaceholder user. Can be used to populate a user profile when prototyping or testing an API.
 *     parameters:
 *       - in: path
 *         name: id
 *         required: true
 *         description: Numeric ID of the user to retrieve.
 *         schema:
 *           type: integer
 *     responses:
 *       200:
 *         description: Success
 *       202:
 *         description: No Action Performed
 *       400:
 *         description: Error
 *       500:
 *         description: Internal Application Domain Error
 *       404:
 *         description: Data Not Found
 */
router.get('/getUser/:id', (req, res, data) => {
  log.info(`[GET] /users/getUser/${req.params.id} data: {}`)
  const {
    id
  } = req.params
  pool.query(
    'SELECT id, name, email FROM users WHERE id = $1',
    [id],
    async (error, results) => {
      if (error) {
        throw error
      }
      res.send(results.rows)
    }
  )
})
/**
 * @swagger
 * /users/createUser:
 *   post:
 *     tags:
 *      - Users
 *     summary: Create a JSONPlaceholder user.
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               name:
 *                 type: string
 *               username:
 *                 type: string
 *               password:
 *                 type: string
 *               role:
 *                 type: string
 *     responses:
 *       200:
 *         description: Success
 *       202:
 *         description: No Action Performed
 *       400:
 *         description: Error
 *       500:
 *         description: Internal Application Domain Error
 *       404:
 *         description: Data Not Found
 */
router.post('/createUser', function (req, res, next) {
  log.info(`[POST] /users data: ${JSON.stringify(req.body)}`)
  const data = {
    name: req.body.name,
    username: req.body.username,
    password: req.body.password,
    role: req.body.role
  }
  pool.query(`INSERT INTO users (name,username,password,role) VALUES ('${req.body.name}','${req.body.username}','${req.body.password}','${req.body.role}')`, async (err, result) => {
    if (err) {
      log.info(err)
      res.json({
        status: 400,
        message: 'Bad Request'
      })
    } else {
      await neo4j.write(
        `CREATE (n:User {name: '${data.name}', username:'${data.username}', password:'${data.password}', role:'${data.role}'}) RETURN n, id(n) as nodeId `, data
      ).then(resp => {
        console.log(resp)
        return res.status(200).send({
          status: '200',
          message: 'user added successfully',
          data
        })
      }).catch(e => next(e))
    }
  })
})

/**
 * @swagger
 * /users/updateData:
 *   put:
 *     tags:
 *      - Users
 *     summary: Retrieve a single JSONPlaceholder user.
 *     description: Retrieve a single JSONPlaceholder user. Can be used to populate a user profile when prototyping or testing an API.
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               name:
 *                 type: string
 *               username:
 *                 type: string
 *               password:
 *                 type: string
 *               role:
 *                 type: string
 *     responses:
 *       200:
 *         description: Success
 *       202:
 *         description: No Action Performed
 *       400:
 *         description: Error
 *       500:
 *         description: Internal Application Domain Error
 *       404:
 *         description: Data Not Found
 */

router.post('/createUser', function (req, res, next) {
  log.info(`[POST] /users data: ${JSON.stringify(req.body)}`)
  const data = {
    name: req.body.name,
    username: req.body.username,
    password: req.body.password,
    role: req.body.role
  }
  console.log(req.body)
  console.log(data)
  // pool.query(`INSERT INTO users (name,username,password,role) VALUES ('${req.body.name}','${req.body.username}','${req.body.password}','${req.body.role}')`, async (err, result) => {
  //   if (err) {
  //     log.info(err)
  //     res.json({
  //       status: 400,
  //       message: 'Bad Request'
  //     })
  //   } else {
  //     await neo4j.write(
  //       `CREATE (n:users {name: '${data.name}', username:'${data.email}'}, password:'${data.password}', role:'${data.role}') RETURN n, id(n) as nodeId `, data
  //     ).then(resp => {
  //       console.log(resp)
  //       return res.status(200).send({
  //         status: '200',
  //         message: 'user added successfully',
  //         data
  //       })
  //     }).catch(e => next(e))
  //   }
  // })
})

/**
 * @swagger
 * /users/deleteUser/{id}:
 *   delete:
 *     tags:
 *      - Users
 *     summary: Retrieve a single JSONPlaceholder user.
 *     description: Retrieve a single JSONPlaceholder user. Can be used to populate a user profile when prototyping or testing an API.
 *     parameters:
 *       - in: path
 *         name: id
 *         required: true
 *         description: Numeric ID of the user to retrieve.
 *         schema:
 *           type: integer
 *     responses:
 *       200:
 *         description: A single user.
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 data:
 *                   $ref: '#/components/schemas/User'
 */
router.delete('/deleteUser/:id', function (req, res) {
  log.info(`[Delete] /users/deleteUser/${req.params.id} data: {}`)
  const {
    id
  } = req.params
  pool.query('DELETE FROM users WHERE id = $1', [id], (error, results) => {
    if (error) {
      throw error
    }
    res.sendStatus(200)
  })
})

module.exports = router
