const express = require('express')
const router = express.Router()
serverUtils = require('../lib/Utils')
serverConfig = require('../lib/initialize')
const log = serverUtils.getLogger('Login')
const pool = require('../lib/config').pool

/**
 * @swagger
 * components:
 *   schemas:
 *     NewUser:
 *       type: object
 *       properties:
 *         name:
 *           type: string
 *           description: The user's name.
 *           example: string
 *         email:
 *           type: string
 *           description: The user's name.
 *           example: string
 *     PutUser:
 *       type: object
 *       properties:
 *         id:
 *           type: string
 *           description: The user's id.
 *           example: ahmed
 *         name:
 *           type: string
 *           description: The user's name.
 *           example: string
 *         email:
 *           type: string
 *           description: The user's name.
 *           example: string
 *     User:
 *       allOf:
 *         - type: object
 *           properties:
 *             id:
 *               type: integer
 *               description: The user ID.
 *               example: 0
 *         - $ref: '#/components/schemas/NewUser'
 */

/**
 * @swagger
 * /login/{username}/{password}:
 *   post:
 *     tags:
 *      - login
 *     summary: Create a JSONPlaceholder user.
 *     parameters:
 *       - in: path
 *         name: username
 *         required: false
 *         description: ex. ahmed
 *         schema:
 *           type: string
 *       - in: path
 *         name: password
 *         required: false
 *         description: ex. 1221
 *         schema:
 *           type: string
 *     responses:
 *       200:
 *         description: Success
 *       202:
 *         description: No Action Performed
 *       400:
 *         description: Error
 *       500:
 *         description: Internal Application Domain Error
 *       404:
 *         description: Data Not Found
 */

router.post('/:username/:password', (req, res, next) => {
  log.info(`[GET] /login/${req.params.username}/${req.params.password} data: ${JSON.stringify(req.params)}`)
  const {
    username
  } = req.params
  const {
    password
  } = req.params
  pool.query(
    'SELECT id,name,username,password,role FROM users WHERE username = $1 AND password = $2',
    [username, password],
    (error, results) => {
      let data = results.rows
      if (results.rows === 0) {
        log.error(error)
        res.json({
          status: 400,
          message: 'Bad Request',
          data
        })
      }
      res.json({
        status: 200,
        message: 'Success',
        data
      })
    }
  )
})

module.exports = router
