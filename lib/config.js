const Pool = require("pg").Pool
serverConfig = require('../lib/initialize')

module.exports = { pool: new Pool(serverConfig.service.postgre) }