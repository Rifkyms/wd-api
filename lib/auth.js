const { Unauthorized } = require('http-errors');

serverConfig = require('../lib/initialize')

module.exports = (req, res, next) => {
  let authheader = req.headers.authorization
  if (!authheader) {
    res.status(401).send('Unauthorized')
  }

  let auth = new Buffer.from(authheader.split(' ')[1],
  'base64').toString().split(':')
  let user = auth[0]
  let pass = auth[1]

  if (user == serverConfig.auth.clientId && pass == serverConfig.auth.clientSecret) {
    next();
  } else {
    res.status(401).send('Unauthorized')
  }
}