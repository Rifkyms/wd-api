/**
 * @swagger
 * components:
 *   schemas:
 *     NewUser:
 *       type: object
 *       properties:
 *         name:
 *           type: string
 *           description: The user's name.
 *           example: string
 *         email:
 *           type: string
 *           description: The user's name.
 *           example: string
 *     PutUser:
 *       type: object
 *       properties:
 *         id:
 *           type: int
 *           description: The user's id.
 *           example: int
 *         name:
 *           type: string
 *           description: The user's name.
 *           example: string
 *         email:
 *           type: string
 *           description: The user's name.
 *           example: string
 *     User:
 *       allOf:
 *         - type: object
 *           properties:
 *             id:
 *               type: integer
 *               description: The user ID.
 *               example: 0
 *         - $ref: '#/components/schemas/NewUser'
 */